Source: python-tesserocr
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Michael Fladischer <fladi@debian.org>,
 Malik Mlitat <mlitat88@gmail.com>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 cython3,
 libleptonica-dev,
 libtesseract-dev,
 pybuild-plugin-pyproject,
 python3-all-dev,
 python3-pil,
 python3-setuptools,
 tesseract-ocr,
Standards-Version: 4.7.0
Homepage: https://github.com/sirfz/tesserocr/
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-tesserocr
Vcs-Git: https://salsa.debian.org/python-team/packages/python-tesserocr.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no
X-Style: black

Package: python3-tesserocr
Architecture: any
Depends:
 tesseract-ocr,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: Python wrapper for the tesseract-ocr API (Python3 version)
 A simple, Pillow-friendly, wrapper around the tesseract-ocr API for Optical
 Character Recognition (OCR).
 .
 tesserocr integrates directly with Tesseract's C++ API using Cython which
 allows for a simple Pythonic and easy-to-read source code. It enables real
 concurrent execution when used with Python's threading module by releasing the
 GIL while processing an image in tesseract.
 .
 tesserocr is designed to be Pillow-friendly but can also be used with image
 files instead.
 .
 This package contains the Python 3 version of the library.
